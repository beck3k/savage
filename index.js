const { app, BrowserWindow } = require('electron')
const fs = require('fs')
const path = require('path')
global.filePath = app.getPath('userData')
global.fieldData = {}
const webcore = require('./webcore.js')

try {
  if(!fs.existsSync(path.join(global.filePath, 'resources'))) {
    fs.mkdirSync(path.join(global.filePath, 'resources'))
  }
} catch (e) {
  fs.mkdirSync(path.join(global.filePath, 'resources'))
}



function createWindow () {
  // Create the browser window.
  let win = new BrowserWindow({
    width: 600,
    height: 300,
    webPreferences: {
      nodeIntegration: true
    }
  })

  // and load the index.html of the app.
  win.loadFile('./index.html')
}

app.on('ready', createWindow)
