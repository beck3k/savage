const express = require('express')
const app = express()
var http = require('http').createServer(app)
var io = require('socket.io')(http)
const fs = require('fs')
const path = require('path')
const gm = require('gray-matter')
const nedb = require('nedb')
const cors = require('cors')
const db = new nedb({ filename: path.join(global.filePath, 'state.nedb'), autoload: true})


function sortObject(o) {
    return Object.keys(o).sort().reduce((r, k) => (r[k] = o[k], r), {});
}

var displayState = {
  '_id': 'displayState',
  screens: [],
  packs: [],
  images: [],
  videos: [],
  sounds: [],
  layers: {},
  variables: {},
  rotators: {},
  current: null,
  currentVideo: '',
  currentPoster: '',
}

db.findOne( {'_id': 'displayState'}, (err, doc) => {
  if(doc) {
    displayState = doc;
  }
});


const port = 9400

app.use(express.json())
app.use(cors())

app.get('/resources', (req, res) => {
  files = fs.readdirSync(path.join(global.filePath, 'resources'))
  files.forEach( file => {
    const ext = path.extname(file)
    const base = path.basename(file)
    if(ext === '.svg') {
      if(!displayState.screens.includes(file)) displayState.screens.push(file)
      var svg = fs.readFileSync(path.join(global.filePath, 'resources', file), 'utf8')

      var matches = Array.from(svg.matchAll(/inkscape:label="([.a-zA-Z0-9- ]+)"?/gi))
      matches.forEach( (m) => {
        if(!Object.keys(displayState.layers).includes(m[1])) {
          displayState.layers[m[1]] = false
        }
      })

      matches = Array.from(svg.matchAll(/data-variable="([.a-zA-Z0-9- ]+)"?/gi))
      matches.forEach( (m) => {
        if(!Object.keys(displayState.variables).includes(m[1])) {
          displayState.variables[m[1]] = ''
        }
      })

      matches = Array.from(svg.matchAll(/data-image="([.a-zA-Z0-9- ]+)"?/gi))
      matches.forEach( (m) => {
        if(!Object.keys(displayState.variables).includes(m[1])) {
          displayState.variables[m[1]] = ''
        }
      })

      matches = Array.from(svg.matchAll(/data-rotator="([a-zA-Z0-9- ]+)"?/gi))
      matches.forEach( (m) => {
        if(!Object.keys(displayState.rotators).includes(m[1])) {
          displayState.rotators[m[1]] = {
            files: [],
            current: 0,
            delay: 10000
          }
        }
      })
    }
    if(ext === '.mp3') {
      if(!displayState.sounds.includes(file)) {
        displayState.sounds.push(file)
      }
    }
    if(ext === '.png' || ext === '.jpg') {
      if(!displayState.images.includes(file)) {
        displayState.images.push(file)
      }
    }
    if(ext === '.mp4' || ext === '.ogv') {
      if(!displayState.videos.includes(file)) {
        displayState.videos.push(file)
      }
    }
    if(ext === '.pack') {
      if(!displayState.packs.includes(file)) {
        packData = gm(fs.readFileSync(path.join(global.filePath, 'resources', file), 'utf8'))
        if(packData && !packData.isEmpty) {
          displayState.packs.push(base)

          if(packData.data['variables']) {
            packData.data.variables.forEach( (v) => {
              if(!Object.keys(displayState.variables).includes(v)) {
                displayState.variables[v] = ''
              }
            })
          }
        }
      }
    }
  })
  displayState.images.sort();
  displayState.videos.sort();
  displayState.sounds.sort();
  displayState.screens.sort();
  displayState.layers = sortObject(displayState.layers)
  displayState.variables = sortObject(displayState.variables)
  res.send(JSON.stringify(displayState))
})

app.get('/packs/:key', (req, res) => {
  packData = gm(fs.readFileSync(path.join(global.filePath, 'resources', req.params['key'] + '.pack'), 'utf8'))

  if(packData && !packData.isEmpty) {
    res.send(packData.content)
  }

})

app.post('/cmd/setText', (req, res) => {
  io.emit('command', {cmd: 'setText', 'key': req.query['key'], value: req.query['value']})
  displayState.variables[req.query['key']] = req.query['value']
  res.send('OK')
})

app.use('/resource-files', express.static(path.join(global.filePath, 'resources')));
app.use('/', express.static(path.join(__dirname, 'src')));

io.on('connection', function(socket){
  console.log('a user connected');

  socket.on('disconnect', function(){
    console.log('user disconnected');
  });

  socket.on('command', function(msg){
    console.log('cmd: ' + JSON.stringify(msg));
    io.emit('command', msg);
    switch(msg.cmd) {
      case 'switch':
        displayState.current = msg.screen
        break
      case 'showLayer':
        displayState.layers[msg.layer] = true
        break
      case 'hideLayer':
        displayState.layers[msg.layer] = false
        break
      case 'setText':
        displayState.variables[msg.key] = msg.value
        break
      case 'updateRotator':
        if(!displayState.rotators[msg.key]) displayState.rotators[msg.key] = {}
        displayState.rotators[msg.key].files = msg.files
        displayState.rotators[msg.key].delay = msg.delay
        break
      case 'loadVideo':
        displayState.currentVideo = msg.video
        break
      case 'playVideo':
        break
      case 'stopVideo':
        break
      case 'setPoster':
        displayState.currentPoster = msg.poster
        break
      case 'saveConfig':
        db.update({ '_id': 'displayState' }, displayState, { upsert: true }, (err, docs, upsert) => {
          if(err) console.log(err)
          console.log(`Updated ${docs}/${upsert} docs`)
        })
        break
    }
  });
});

http.listen(port, function(){
  console.log(`webcore listening on *:${port}`);
  console.log(`serving resources from ${global.filePath}`)
});
